<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'reglow' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Lf9lP>3_mY3_,g.1^!|/ND8t2Zy&NOy.CF0W/:7V(Y8*Io3F#dcWu%@)t|h&^EI:' );
define( 'SECURE_AUTH_KEY',  'C}WHA`X,jWi5>iFw-Up@d%)7g9=4Ql+Cx-ZdXchJ#{#Jqkh(QB<jp~2C[h>8$_LW' );
define( 'LOGGED_IN_KEY',    ',QR`[xHhf&bW)3C/Buy&EiUstyxob`2]]7z_u|}K^Ua+&JJw!R#$TO(f?k%! waJ' );
define( 'NONCE_KEY',        'LQ~K.j7byTMNs6xdLe7Jc2j@b9{n;qwJc,]u,CO,7%9pvh)UKHmei?m&HD:FNGUG' );
define( 'AUTH_SALT',        ':UvReWmt{JlHjHfoxl9Aj5;+lO5@CbKX^koG96C4nz>~0n;+[nP)n!KEA4+6J5Y(' );
define( 'SECURE_AUTH_SALT', 'yVT2ZbW*w~c?gXo @KlEmjp[`#u>2d Vbz8WdstZ}MplNird`?FYspZ3EA7B:+/Y' );
define( 'LOGGED_IN_SALT',   '7pRkR1X HsY02gp3}:tksj@9nq0~m2ey^0[-Ynzs_4*z5TI5UCz[d/p@4Zg?5|@d' );
define( 'NONCE_SALT',       '@p@jvLapr4CwI]]`5pKq;O5hf}t(,T2R!ibYsuru%!U):L0X#a$PSz-+qsE]24[5' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
